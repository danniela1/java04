package deloitte.academy.lesson01.logic;

import deloitte.academy.lesson01.entity.Persona;
import deloitte.academy.lesson01.run.Run;

public class Registro extends Persona {
	
	public Registro() {
		// TODO Auto-generated constructor stub
	}
	
	public Registro(int idPersona, double precio, int numH, HuespedT huespedT) {
		super(idPersona, precio, numH, huespedT);
		// TODO Auto-generated constructor stub
	}

	
	/**
	 * metodo que se encarga de realizar el descuento apropiado dependiendo del tipo de cliente
	 */
	public double importe() {
		double total = this.getPrecio();
		
		if(this.getHuespedT().equals(HuespedT.Empleado))
		{
			total = this.getPrecio() * .50;
		}
		if(this.getHuespedT().equals(HuespedT.HuespedF))
		{
			total = this.getPrecio() * .30;
		}
		
		return total;

	}
	
	/**
	 * metodo que se encarga de realizar el checkint del cliente
	 * @param registrop
	 * @return
	 */
	public String registroPersona(Registro registrop) 
	{
		
		String mensaje="";
		registrop = Run.huespedes.get(0);    //Igualas el producto al primer elemento de la lista y ya puedes empezar a hacer las comparaciones.
	    for (Registro x: Run.huespedes)
	    {
	    	if(x.getNumH() == registrop.getNumH()) 
	    	{
	    		mensaje = "no disponible";// pro.getPrecio()) pro = produc;
	    		break;
	    	}

	    	else {
	    		mensaje = "Disponible"; Run.huespedes.add(x);
				break; 
	    	}
	    
	    }
	    return mensaje;
	}
	
	/**
	 * metodo que se encarga de realizar el checkout del cliente
	 * @param registrop 
	 * @return 
	 */
	public String checkOutPersona(Registro registrop) 
	{
		String mensaje="";
		for (Registro x: Run.huespedes)
	    {
	    	if(x.getIdPersona() == registrop.getIdPersona()) 
	    	{
	    		mensaje = "CheckOut realizado"; Run.huespedes.remove(x);
				break; 
	    	}
	    	
	    	else {
	    		mensaje = "registro no encontrado"; 
				break; 
	    	}
	    }
	        
	return mensaje;
	}

}
