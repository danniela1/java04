package deloitte.academy.lesson01.run;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import deloitte.academy.lesson01.entity.Persona;
import deloitte.academy.lesson01.logic.HuespedT;
import deloitte.academy.lesson01.logic.Registro;

public class Run {

	public static final List<Registro> huespedes = new ArrayList<Registro>();
	
	private static final Logger LOGGER = Logger.getLogger(Run.class.getName());
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Registro r1 = new Registro(1,100,2, HuespedT.HuespedF);
		Registro r2 = new Registro(2,100,5, HuespedT.HuespedF);
		Registro r3 = new Registro(3,100,7, HuespedT.Huesped);
		Registro r4 = new Registro(4,150,6, HuespedT.Empleado);
		Registro r5 = new Registro(5,200,1, HuespedT.Huesped);
		Registro r6 = new Registro(6,200,21, HuespedT.Huesped);
		huespedes.add(r1);
		huespedes.add(r4);
		huespedes.add(r5);
		huespedes.add(r2);
		//huespedes.add(r6);
		
		
		LOGGER.info("Registro Actual General:");
		
		for (Registro p : huespedes) {
			System.out.println("id: " + p.getIdPersona() + " N.H." + p.getNumH() + " Costo:" + p.getPrecio());
			}
		
		
		//checkIn
		Registro huesped = new Registro();
		String a = huesped.registroPersona(r3);
		LOGGER.info("CheckInt: " + a);
		
		//checkOut
		Registro huesped2 = new Registro();
		String b = huesped2.checkOutPersona(r6);
		LOGGER.info("CheckOut: " + b); 
		
		
		
		//pago
		ejecutarPago(r1);
		ejecutarPago(r4);
		ejecutarPago(r5);
	}
	
	public static void ejecutarPago(Persona p) {
		LOGGER.info("total a pagar con descuento : " + p.importe() + " id de persona: " 
	+p.getIdPersona() + "  tipo huesped: " + p.getHuespedT());
	}

}
