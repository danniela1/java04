package deloitte.academy.lesson01.entity;

import deloitte.academy.lesson01.logic.HuespedT;

public abstract class Persona {
	
	private int idPersona;
	private double precio;
	private int numH;
	private HuespedT huespedT;
	
	public Persona() {
		// TODO Auto-generated constructor stub
	}

	
	
	public Persona(int idPersona, double precio, int numH, HuespedT huespedT) {
		super();
		this.idPersona = idPersona;
		this.precio = precio;
		this.numH = numH;
		this.huespedT = huespedT;
	}

	public int getIdPersona() {
		return idPersona;
	}



	public void setIdPersona(int idPersona) {
		this.idPersona = idPersona;
	}



	public double getPrecio() {
		return precio;
	}



	public void setPrecio(double precio) {
		this.precio = precio;
	}



	public int getNumH() {
		return numH;
	}



	public void setNumH(int numH) {
		this.numH = numH;
	}



	public HuespedT getHuespedT() {
		return huespedT;
	}



	public void setHuespedT(HuespedT huespedT) {
		this.huespedT = huespedT;
	}



	public abstract double importe();

}
